# dwm - dynamic window manager

Version `6.4` for `FreeBSD` with the following patches applied:

- `pertag`
- `noborder floating fix`
- `noborder self flicker fix`
- `always center`
- `hide vacant tags`

